package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"

	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"

	pb "gitlab.com/schin1/ai-assist/protos/headermirror"
)

// server is used to implement example.HeaderServiceServer.
type server struct {
	pb.UnimplementedHeaderServiceServer
	db  *sql.DB
	alt *sql.DB
}

// GetHeaders implements example.HeaderServiceServer
func (s *server) GetHeaders(ctx context.Context, in *pb.Empty) (*pb.HeaderResponse, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return &pb.HeaderResponse{Headers: map[string]string{}}, nil
	}

	headers := make(map[string]string)
	for key, values := range md {
		headers[key] = values[0] // Assuming single value for simplicity
	}

	err := s.db.PingContext(ctx)
	if err != nil {
		return &pb.HeaderResponse{Headers: headers}, err
	}

	err = s.alt.PingContext(ctx)
	if err != nil {
		return &pb.HeaderResponse{Headers: headers}, err
	}

	headers["dbping"] = "success"

	return &pb.HeaderResponse{Headers: headers}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	db1, err := ConnectUnixSocket("DB_USER", "DB_PASS", "DB_NAME")
	if err != nil {
		log.Fatalf("db1 failed to connect: %v", err)
	}

	_ = db1.PingContext(context.Background())

	s := grpc.NewServer()
	pb.RegisterHeaderServiceServer(s, &server{db: db1, alt: db1})

	healthServer := health.NewServer()
	healthpb.RegisterHealthServer(s, healthServer)
	healthServer.SetServingStatus("headermirror.HeaderService", healthpb.HealthCheckResponse_SERVING)

	reflection.Register(s)

	log.Printf("Server listening on port 8080")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func ConnectUnixSocket(userEnv, pwdEnv, nameEnv string) (*sql.DB, error) {
	mustGetenv := func(k string) string {
		v := os.Getenv(k)
		if v == "" {
			log.Fatalf("Fatal Error in connect_unix.go: %s environment variable not set.\n", k)
		}
		return v
	}

	var (
		dbUser = mustGetenv(userEnv) // e.g. 'my-db-user'
		dbPwd  = mustGetenv(pwdEnv)  // e.g. 'my-db-password'
		dbName = mustGetenv(nameEnv) // e.g. 'my-database'
	)

	return connectUnixSocket(dbUser, dbPwd, dbName)
}

func connectUnixSocket(user, pwd, name string) (*sql.DB, error) {
	dbURI := fmt.Sprintf("user=%s password=%s database=%s host=127.0.0.1 port=5000",
		user, pwd, name)

	// dbPool is the pool of database connections.
	dbPool, err := sql.Open("pgx", dbURI)
	if err != nil {
		return nil, fmt.Errorf("sql.Open: %w", err)
	}

	configureConnectionPool(dbPool)

	return dbPool, nil
}

func configureConnectionPool(db *sql.DB) {
	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(7)
	db.SetConnMaxLifetime(1800 * time.Second)
}
